from django.test import TestCase


class ModelTestCase(TestCase):
    def fields_to_dict(self, model_class):
        return dict([(f.name, f) for f in model_class._meta.fields])

    def field_list(self, model_class):
        return [f.name for f in model_class._meta.fields]

    def get_field(self, model_class, field_name):
        return self.fields_to_dict(model_class).get(field_name, None)

    def assertHasField(self, model_class, field_name):
        self.assertTrue(
            field_name in self.field_list(model_class),
            '%s has no field %s' % (model_class, field_name)
        )

    def assertHasNoField(self, model_class, field_name):
        self.assertFalse(
            field_name in self.field_list(model_class),
            '%s should not have field %s' % (model_class, field_name)
        )

    def assertFieldType(self, model_class, field_name, field_class):
        self.assertEqual(
            type(self.fields_to_dict(model_class).get(field_name, None)),
            field_class,
            'field %s is not of type %s' % (field_name, field_class)
        )

    def assertNotFieldType(self, model_class, field_name, field_class):
        self.assertNotEqual(
            type(self.fields_to_dict(model_class).get(field_name, None)),
            field_class,
            'field %s should not be of type %s' % (field_name, field_class)
        )

    def assertForeignKey(self, model_class, field_name, related_model):
        field = self.get_field(model_class, field_name)
        self.assertEqual(
            field.related.parent_model,
            related_model,
            'Field %s does not point to %s' % (field_name, related_model)
        )