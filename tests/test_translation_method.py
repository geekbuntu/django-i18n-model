# -*- coding: utf8 -*-

""" Tests the translate() class method
"""

from __future__ import unicode_literals

from django.test import TestCase
from django.conf import settings
from django.core.management import call_command
from django.db.models import loading
from django.utils import translation

from .models import Post, PostI18N

# Run syncdb manually
loading.cache.loaded = False
call_command('syncdb', verbosity=0)


class TranslateTestCase(TestCase):
    language_codes = [l[0] for l in settings.LANGUAGES]

    def setUp(self):
        self.post = Post.objects.create(
            title='Test title',
            body='This is a test post',
            location='Someplace'
        )

    def tearDown(self):
        Post.objects.all().delete()

    def test_basic_translation(self):
        """
        Test the basic translation workflow
        """
        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        # Count the created objects
        self.assertEqual(PostI18N.objects.count(), 2)

    def test_retrieve_translations(self):
        """
        Test retrieving translations using translate class method
        """
        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        translation_de = PostI18N.translate(self.post, 'de')
        translation_it = PostI18N.translate(self.post, 'it')

        self.assertEqual(translation_de.title, 'Test-Titel')
        self.assertEqual(translation_de.body, 'Dies ist ein Test Beitrag')
        self.assertEqual(translation_it.title, 'Titolo di prova')
        self.assertEqual(translation_it.body, 'Questo è un post di prova')

    def test_translations_on_related_object(self):
        """
        Test retrieving translations from source object
        """
        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        self.assertEqual(
            self.post.translations.de().get(),
            PostI18N.translate(self.post, 'de')
        )
        self.assertEqual(
            self.post.translations.it().get(),
            PostI18N.translate(self.post, 'it')
        )

    def test_get_current_language(self):
        """
        Test getting current language
        """

        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        translation.activate('de')

        translation_de = PostI18N.translate(self.post, 'de')

        self.assertEqual(
            self.post.translations.lang().get(),
            translation_de
        )

    def test_get_arbitrary_langauge_programmatically(self):
        """
        Test getting language using the ``langauge`` manager method
        """

        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        translation.activate('it')
        translation_de = PostI18N.translate(self.post, 'de')

        self.assertEqual(
            self.post.translations.lang('de').get(),
            translation_de
        )

        self.assertEqual(
            self.post.translations.de().get(),
            self.post.translations.lang('de').get()
        )