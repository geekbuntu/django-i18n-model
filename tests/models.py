""" Test models and also an example of translatable model
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _

from i18n_model.models import I18nModel


__all__ = (
    'Post', 'PostI18N', 'Book', 'BookTranslation', 'Article',
    'ArticleTranslation', 'News', 'NewsTranslation'
)


class Post(models.Model):
    title = models.CharField(
        _('title'),
        max_length=100
    )
    body = models.TextField(
        _('body'),
        max_length=200
    )
    location = models.CharField(
        _('location'),
        max_length=150
    )
    created_at = models.DateTimeField(
        _('timestamp'),
        auto_now_add=True,
        editable=False
    )

    def __unicode__(self):
        return self.title


class PostI18N(I18nModel):
    class Meta:
        translation_fields = ('title', 'body')


class Book(models.Model):
    title = models.CharField(
        _('title'),
        max_length=100
    )
    author = models.CharField(
        _('author'),
        max_length=100
    )
    year = models.PositiveSmallIntegerField(
        _('year of publication')
    )
    isbn = models.CharField(
        _('ISBN'),
        max_length=13
    )


class BookTranslation(I18nModel):
    class Meta:
        source_model = Book
        translation_fields = ('title',)


class Article(models.Model):
    title = models.CharField(
        _('title'),
        max_length=100
    )
    author = models.CharField(
        _('author'),
        max_length=100
    )
    body = models.TextField(
        _('body')
    )
    created_at = models.DateTimeField(
        _('publication timestamp'),
        auto_now_add=True,
        editable=False
    )


class ArticleTranslation(I18nModel):
    class Meta:
        source_model = 'Article'


class News(models.Model):
    heading = models.CharField(
        _('heading'),
        max_length=100
    )
    lead = models.CharField(
        _('lead'),
        max_length=200
    )
    article = models.TextField(
        _('article body')
    )
    created_at = models.DateTimeField(
        _('creation timestamp'),
        auto_now_add=True,
        editable=False
    )
    updated_at = models.DateTimeField(
        _('last update timestamp'),
        auto_now=True,
        editable=False
    )


class NewsTranslation(I18nModel):
    class Meta:
        source_model = 'tests.News'