""" Test that translation model has correct features after construction
"""

from django.conf import settings
from django.db import models

from .models import Post, PostI18N
from .model_test_case import ModelTestCase


class I18nModelTestCase(ModelTestCase):
    """ Basic tests for the I18nModel class
    """

    def test_translated_fields(self):
        """
        Test that the PostI18N has correct title and body fields
        """
        self.assertHasField(PostI18N, 'title')
        self.assertFieldType(PostI18N, 'title', models.CharField)
        self.assertHasField(PostI18N, 'body')
        self.assertFieldType(PostI18N, 'body', models.TextField)

    def test_non_translated_fields(self):
        """
        Test that PostI18N doesn't have non-translated fields
        """
        self.assertHasNoField(PostI18N, 'location')
        self.assertHasNoField(PostI18N, 'created_at')

    def test_translation_fields(self):
        """
        Test that PostI18N has additional translation-specific fields
        """
        self.assertHasField(PostI18N, 'i18n_source')
        self.assertFieldType(PostI18N, 'i18n_source', models.ForeignKey)
        self.assertHasField(PostI18N, 'i18n_language')
        self.assertFieldType(PostI18N, 'i18n_language', models.CharField)

    def test_correct_source(self):
        """
        Test that PostI18N points to the correct source model
        """
        self.assertForeignKey(PostI18N, 'i18n_source', Post)

    def test_correct_lang_choices(self):
        """
        Test that PostI18N uses language choices that correspond to LANGUAGES
        """
        lang_field = self.get_field(PostI18N, 'i18n_language')
        self.assertEqual(lang_field.choices, settings.LANGUAGES)