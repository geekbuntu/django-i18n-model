# -*- coding: utf8 -*-

""" Tests the i18n_model template tags
"""

from __future__ import unicode_literals

from django.test import TestCase
from django.conf import settings
from django.core.management import call_command
from django.db.models import loading
from django.utils import translation

from i18n_model.templatetags.i18n_model import translate

from .models import Post, PostI18N

# Run syncdb manually
loading.cache.loaded = False
call_command('syncdb', verbosity=0)


class TranslateTestCase(TestCase):
    language_codes = [l[0] for l in settings.LANGUAGES]

    def setUp(self):
        self.post = Post.objects.create(
            title='Test title',
            body='This is a test post',
            location='Someplace'
        )

    def tearDown(self):
        Post.objects.all().delete()

    def test_translate_tag_basics(self):
        """
        Test the default scenario for the ``translate`` tag
        """
        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        translation_it = PostI18N.translate(self.post, 'it')
        translation.activate('it')

        self.assertEqual(
            translate(self.post),
            translation_it
        )

    def test_translate_tag_with_args(self):
        """
        Test the translate tag with argument
        """
        PostI18N.translate(
            self.post,
            'de',
            title='Test-Titel',
            body='Dies ist ein Test Beitrag',
        )
        PostI18N.translate(
            self.post,
            'it',
            title='Titolo di prova',
            body='Questo è un post di prova'
        )

        translation_it = PostI18N.translate(self.post, 'it')
        translation_de = PostI18N.translate(self.post, 'de')

        self.assertEqual(
            translate(self.post, 'it'),
            translation_it
        )

        self.assertEqual(
            translate(self.post, 'de'),
            translation_de
        )

    def test_missing_translation(self):
        """
        Test that original is returned when there is no translation
        """

        self.assertEqual(
            translate(self.post),
            self.post
        )

        self.assertEqual(
            translate(self.post, 'ru'),
            self.post
        )